
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `xv5m_posts`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xv5m_posts` (
  `ID` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `post_author` bigint(20) unsigned NOT NULL DEFAULT '0',
  `post_date` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_date_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_title` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_excerpt` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'publish',
  `comment_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `ping_status` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'open',
  `post_password` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `post_name` varchar(200) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `to_ping` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `pinged` mediumtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_modified` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_modified_gmt` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `post_content_filtered` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `post_parent` bigint(20) unsigned NOT NULL DEFAULT '0',
  `guid` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `menu_order` int(11) NOT NULL DEFAULT '0',
  `post_type` varchar(20) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT 'post',
  `post_mime_type` varchar(100) COLLATE utf8mb4_unicode_ci NOT NULL DEFAULT '',
  `comment_count` bigint(20) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ID`),
  KEY `type_status_date` (`post_type`,`post_status`,`post_date`,`ID`),
  KEY `post_parent` (`post_parent`),
  KEY `post_author` (`post_author`),
  KEY `post_name` (`post_name`(191))
) ENGINE=MyISAM AUTO_INCREMENT=11 DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `xv5m_posts` WRITE;
/*!40000 ALTER TABLE `xv5m_posts` DISABLE KEYS */;
INSERT INTO `xv5m_posts` VALUES (1,1,'2018-04-24 15:43:53','2018-04-24 15:43:53','Welcome to WordPress. This is your first post. Edit or delete it, then start blogging!','Hello world!','','publish','open','open','','hello-world','','','2018-04-24 15:43:53','2018-04-24 15:43:53','',0,'http://wpgit.skryptersi.atthouse.pl/?p=1',0,'post','',1),(2,1,'2018-04-24 15:43:53','2018-04-24 15:43:53','This is an example of a WordPress page, you could edit this to put information about yourself or your site so readers know where you are coming from. You can create as many pages like this one or sub-pages as you like and manage all of your content inside of WordPress.','About','','publish','open','open','','about','','','2018-04-24 15:43:53','2018-04-24 15:43:53','',0,'http://wpgit.skryptersi.atthouse.pl/?page_id=2',0,'page','',0),(3,1,'2018-04-24 16:13:15','0000-00-00 00:00:00','','Automatycznie zapisany szkic','','auto-draft','open','open','','','','','2018-04-24 16:13:15','0000-00-00 00:00:00','',0,'http://wpgit.skryptersi.atthouse.pl/?p=3',0,'post','',0),(5,1,'2018-04-24 17:15:08','2018-04-24 17:15:08','hello world','Wpis #2','','publish','open','open','','wpis-2','','','2018-04-24 17:15:08','2018-04-24 17:15:08','',0,'http://wpgit.skryptersi.atthouse.pl/?p=5',0,'post','',0),(6,1,'2018-04-24 17:15:08','2018-04-24 17:15:08','hello world','Wpis #2','','inherit','closed','closed','','5-revision-v1','','','2018-04-24 17:15:08','2018-04-24 17:15:08','',5,'http://wpgit.skryptersi.atthouse.pl/uncategorized/5-revision-v1/',0,'revision','',0),(7,1,'2018-04-24 17:15:32','2018-04-24 17:15:32','hello world dwa zero','Wpis #3','','publish','open','open','','wpis-3','','','2018-04-24 17:15:32','2018-04-24 17:15:32','',0,'http://wpgit.skryptersi.atthouse.pl/?p=7',0,'post','',0),(8,1,'2018-04-24 17:15:32','2018-04-24 17:15:32','hello world dwa zero','Wpis #3','','inherit','closed','closed','','7-revision-v1','','','2018-04-24 17:15:32','2018-04-24 17:15:32','',7,'http://wpgit.skryptersi.atthouse.pl/uncategorized/7-revision-v1/',0,'revision','',0),(9,1,'2018-04-24 17:17:28','2018-04-24 17:17:28','Dodanie kolejnego wpis do sprawdzenia czy działa to tak jak myślę.','Wpis #4','','publish','open','open','','wpis-4','','','2018-04-24 17:17:28','2018-04-24 17:17:28','',0,'http://wpgit.skryptersi.atthouse.pl/?p=9',0,'post','',0),(10,1,'2018-04-24 17:17:28','2018-04-24 17:17:28','Dodanie kolejnego wpis do sprawdzenia czy działa to tak jak myślę.','Wpis #4','','inherit','closed','closed','','9-revision-v1','','','2018-04-24 17:17:28','2018-04-24 17:17:28','',9,'http://wpgit.skryptersi.atthouse.pl/uncategorized/9-revision-v1/',0,'revision','',0);
/*!40000 ALTER TABLE `xv5m_posts` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

