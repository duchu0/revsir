
/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;
DROP TABLE IF EXISTS `xv5m_revisr`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `xv5m_revisr` (
  `id` mediumint(9) NOT NULL AUTO_INCREMENT,
  `time` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `message` text,
  `event` varchar(42) NOT NULL,
  `user` varchar(60) DEFAULT NULL,
  UNIQUE KEY `id` (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

LOCK TABLES `xv5m_revisr` WRITE;
/*!40000 ALTER TABLE `xv5m_revisr` DISABLE KEYS */;
INSERT INTO `xv5m_revisr` VALUES (1,'2018-04-24 17:08:54','Successfully created a new repository.','init','admin'),(2,'2018-04-24 17:10:00','Successfully backed up the database.','backup','admin'),(3,'2018-04-24 17:10:13','Committed <a href=\"http://wpgit.skryptersi.atthouse.pl/wp-admin/admin.php?page=revisr_view_commit&commit=cb03272&success=true\">#cb03272</a> to the local repository.','commit','admin'),(4,'2018-04-24 17:10:13','Error pushing changes to the remote repository.','error','admin'),(5,'2018-04-24 17:12:27','Error pushing changes to the remote repository.','error','admin'),(6,'2018-04-24 17:13:40','Error pushing changes to the remote repository.','error','admin'),(7,'2018-04-24 17:15:52','Successfully backed up the database.','backup','admin'),(8,'2018-04-24 17:15:52','Committed <a href=\"http://wpgit.skryptersi.atthouse.pl/wp-admin/admin.php?page=revisr_view_commit&commit=4c662de&success=true\">#4c662de</a> to the local repository.','commit','admin'),(9,'2018-04-24 17:15:53','Error pushing changes to the remote repository.','error','admin'),(10,'2018-04-24 17:52:17','Successfully imported the database. ','import','admin'),(11,'2018-04-24 17:52:17','Successfully reverted the database to a previous commit. <a href=\"http://wpgit.skryptersi.atthouse.pl/wp-admin/admin-post.php?action=process_revert&amp;revert_type=db&amp;db_hash=b1b5710&amp;revisr_revert_nonce=e5a5317950\">Undo</a>','revert','admin'),(12,'2018-04-24 17:57:17','There was an error committing the changes to the local repository.','error','admin'),(13,'2018-04-24 17:57:23','Committed <a href=\"http://wpgit.skryptersi.atthouse.pl/wp-admin/admin.php?page=revisr_view_commit&commit=25bdaf2&success=true\">#25bdaf2</a> to the local repository.','commit','admin'),(14,'2018-04-24 17:57:23','Error pushing changes to the remote repository.','error','admin'),(15,'2018-04-24 17:57:30','Error pushing changes to the remote repository.','error','admin'),(16,'2018-04-24 18:04:54','Error pushing changes to the remote repository.','error','admin'),(17,'2018-04-24 18:08:38','Error pushing changes to the remote repository.','error','admin'),(18,'2018-04-24 18:12:35','Error pushing changes to the remote repository.','error','admin'),(19,'2018-04-24 18:13:21','Error pushing changes to the remote repository.','error','admin'),(20,'2018-04-24 18:14:07','Created new branch: test','branch','admin'),(21,'2018-04-24 18:19:04','Successfully imported the database. ','import','admin'),(22,'2018-04-24 18:19:13','Error pushing changes to the remote repository.','error','admin');
/*!40000 ALTER TABLE `xv5m_revisr` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

