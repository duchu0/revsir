<?php

// Configuration common to all environments
include_once __DIR__ . '/wp-config.common.php';

/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('REVISR_WORK_TREE', '/home/skryptersi/websites/wp_1/'); // Added by Revisr
define('REVISR_GIT_PATH', ''); // Added by Revisr
define('DB_NAME', '1627_wp_1');

/** MySQL database username */
define('DB_USER', '1627_wp_1');

/** MySQL database password */
define('DB_PASSWORD', 'Xc8ngvou');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'XOVXhptQ5I6eRAGPvBSdq6Akp7kLYjBMYB0MLULQF6GiGqecVHWS8gao9YagxAZX');
define('SECURE_AUTH_KEY',  'ElSTAkSy49hUpcyN2PKi4K5BZi5j9ACbKZPNR64U7lOVQ3IoUZx0xzDz8qo4HMO7');
define('LOGGED_IN_KEY',    'KtJJYbzQZIIAngWfVwTWhcyMHS3AQuWYRc5CcMyLou7HyS7jwBxH8KZuYKdE4xg7');
define('NONCE_KEY',        'Xr9oTtneHunk4iEDxkEO5FhgKkGh4IQzpXJWjX4pH2aEVoIKcZXiKqPnLtlGuGmD');
define('AUTH_SALT',        'q3qN3Hj7ebENEeIgxkQFqtY7fNNhQ2MefB6UaJWp2Y0X6sim5A24M5MORbUo1Yzp');
define('SECURE_AUTH_SALT', 'ifpEN1w8wMXkoWAj69no9migUqZhqEevtua85J9eBHBueQPUGXBQHkVzQTKH3vVD');
define('LOGGED_IN_SALT',   'dqUWlhKgZw9PmIXR30hqpTdH2LUJgatQbL06r2sBTjNW1VpLyvvR8S1zQbjnzHzC');
define('NONCE_SALT',       'MZ5jJKhpfFYospK3CgqkbmR5o30AoAsSYMCuY6PuxKKiQh85G5l72lnKhKVEk8ZY');

/**
 * Other customizations.
 */
define('FS_METHOD','direct');define('FS_CHMOD_DIR',0755);define('FS_CHMOD_FILE',0644);
define('WP_TEMP_DIR',dirname(__FILE__).'/wp-content/uploads');

/**
 * Turn off automatic updates since these are managed upstream.
 */
define('AUTOMATIC_UPDATER_DISABLED', true);


/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'xv5m_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
